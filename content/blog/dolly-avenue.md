---
title: "Dolly Avenue"
date: 2020-06-05
slug: "dolly-avenue"
description: "Villa's ready to move near vengambakkam"
keywords: ["villa", "dolly", "avenue"]
draft: false
tags: ["completed"]
math: false
toc: false
---
### Features
1. **STRUCTURE**
    * RCC frame structure with clay bricks. 
1. **PAINTING**
    * Interior: Superior Quality Interior Plastic Emulsion Paint.
    * Exterior: Weather Proof Exterior Emulsion or equivalent. 
1. **FLOORING**
    * Hall, Living Area and Bedrooms : 2X2 Vitrified flooring or equivalent.
    * Kitchen : Anti-skid Vitrified or equivalent flooring.
    * Utility and Balcony : Anti-skid ceramic flooring
    * Toilets : Anti-skid Ceramic tiles/vitrified or equivalent for flooring and wall &doing.
    * Staircase : Granite steps with MS handrail. 
1. **PLUMBING**
    * All Sanitary wares shall be from Parryware or of equivalent make. 
    * All concealed water supply lines of CPVC or equivalent and PVC lines for external. 
    * Individual OHT provision for each villa. 
1. **DOORS AND WINDOWS**
    * Main door : Solid teak wood door frame with paneled shutter.
    * Other room doors : Good quality flush doors.
    * Toilet doors : PVC Sintex or equivalent. 
1. **WINDOWS & VENTILATORS**
    * All windows will be provided with UPVC windows with pin head glass. 
1. **KITCHEN / UTILITY**
    * Provision for electrical & plumbing points.
    * Granite platform with stainless steel sink of reputed make. Dodoing upto 2 ft above cooking platform in kitchen with ceramic tiles 
1. **ELECTRICAL** 
    * Adequate points for fans and lights, TV and appliances
    * Branded modular switches. 
1. **AC PROVISIONS** 
    * Provision for split AC in all bedrooms. 
1. **OTHER PROVISIONS** 
    * Individual Borewell with adequate depth for 24 hours water supply. 
    * Individual water sump, septic tank and soak pit. 
    * Individual overhead tanks. 
    * Covered Car Parking.
 
### Photo Gallery
{{< instagram CCiEKVSAwey hidecaption >}}

### Plan Details
{{< instagram CCiItUbg85c hidecaption >}}

### Access Map
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1944.7168340583473!2d80.13496070057592!3d12.879812399197073!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3a525971261b3ddb%3A0x31c2913c26ad460e!2sDolly%20Avenue!5e0!3m2!1sen!2sin!4v1594537261419!5m2!1sen!2sin" width="600" height="450" frameborder="0" style="border:0"></iframe>

### Pricing Info
| S. No | Item | Details | 
| ----- | ---- | ------- | 
| 1. | Price | 65 Lakhs only * |
| 2. | Status | Ready to move in | 
| 3. | Unit Details | 3 BHK Villa |

 _Price does not include taxes, registration and other applicable charges (e.g. EB and interiors)._ 
